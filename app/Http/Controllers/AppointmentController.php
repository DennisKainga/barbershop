<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Appointment;
use App\Models\AppointmentService;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($barber=null){
    
        // dd($barber);
        $user_id = Auth::user()->id;
        $account_id = Account::where("account_login_id",$user_id)->first()->id;
        // dd($account_id);
                
        if($barber == null){
            $appointment = null;
            $appointment = Appointment::where("appointment_customer_id",$account_id)->where("appointment_status","!=",4)->first();
            // dd($user_id);
            $selected_services = null;
            if($appointment !=null){
                $appointment_services = AppointmentService::where("appointment_service_appointment_id", $appointment->id)->pluck("appointment_service_service_id")->toArray();
                $selected_services = Service::whereIn("id", $appointment_services)->get();
            }
            // dd($appointment_services);
            $barbers = Account::whereHas('user', function($query) {
                $query->where('login_rank', 1);
            })->get();
            
            $services = Service::all();

            // dd($appointment);
            return view("appointments.customer",[

                "barbers"=>  $barbers,
                "services"=> $services,
                "appointment"=>$appointment,
                'selected_services'=>$selected_services
            
            ]);        
        }

        else{
            // dd(Appointment::where("appointment_barber_id",$account_id)->get());
            return view("appointments.barber",[
                "appointments"=>Appointment::where("appointment_barber_id",$account_id)->get()
            ]);
        }
    }
    

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request){

        $FormFields = $request->validate([
            "appointment_date"=>["required"],
            "barber"=>["required"]
        ]);
        $user_id = Auth::user()->id;
        $account_id = Account::where("account_login_id",$user_id)->first()->id;
        Appointment::create([
            "appointment_customer_id"=>$account_id,
            "appointment_barber_id"=>$FormFields["barber"],
            "appointment_date"=>$FormFields["appointment_date"],
            "appointment_status"=>1
        ]);

        return redirect()->back();
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }


    public function finalize(Appointment $appointment){
        $appointment->appointment_status = 2;
        $appointment->save();
        return redirect()->back();
    }

    public function approve(Appointment $appointment){
        $appointment->appointment_status = 3;
        $appointment->save();
        return redirect()->back();

    }
}
