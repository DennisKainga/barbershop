<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $fillable = [

        "service_name",
        "service_image",
        "service_price",
     
    
    ];

    public function appointment_service(){
        return $this->hasMany(AppointmentService::class,"appointment_service_service_id");
    }

      
}
