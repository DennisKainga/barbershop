<x-Layout>

    <style>
    
        .card {
            margin: 60px auto;
            border: none !important;
            width: 450px;
        }
    
        @media screen and (max-width: 768px) {
            .card {
                width: 350px;
            }
        }
    
        .card-strip {
            border-bottom: 1px solid lightgray;
            padding-top: 20px;
            padding-bottom: 20px;
        }
    
        img {
            width: 60px;
            height: 60px;
            border-radius: 50%;
            cursor: pointer;
        }
    
        .sm-text {
            font-size: 15px;
        }
    
        .fa {
            font-size: 30px;
            padding: 10px;
        }
    
        .fa-comment-o {
            color: gray;
            cursor: pointer;
        }
    
        .fa-phone {
            color: #1E88E5;
            padding-left: 20px;
            cursor: pointer;
        }
    
        .time {
            border: 1px lightgray solid;
            border-radius: 5px;
            font-weight: bold;
            padding: 1px 8px;
            margin: 0px 6px;
        }
    
        .price {
            font-size: 20px;
        }
    
        .text-muted {
            color: #BDBDBD !important;
        }
    
        button:focus {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
            outline-width: 0;
        }
    
        .btn {
            width: 48%;
            height: 55px;
            margin: 20px 0px;
            font-weight: bold;
            letter-spacing: 1px;
        }
    
        .btn-white {
            background-color: #fff;
            color: #000;
            border: 1px gray solid;
        }
    
        .btn-white:hover {
            background-color: lightgray;
            color: #000;
        } 
    
        .btn-purple {
            background-color: #5E35B1;
            color: #fff;
            border: 1px #5E35B1 solid;
        }
    
        .btn-purple:hover {
            background-color: #311B92;
            color: #fff;
        }
    </style>
        

        
        <div class="container-fluid px-1 py-5">
            @foreach ($appointments as $appointment)
                <div class="row d-flex justify-content-center ">
                    <div class="card shadow-lg p-3 mb-5 bg-white rounded">
                        <div class="row d-flex justify-content-between mx-2 px-3 card-strip">
                            <div class="left d-flex flex-column">
                                {{-- <h5 class="mb-1">10:00 - 11:00 AM</h5> --}}
                                <p class="mb-1 sm-text">Date: {{ $appointment->appointment_date }}</p>
                            </div>
                        </div>
                        <div class="row d-flex justify-content-between mx-2 px-3 card-strip">
                            <div class="left d-flex flex-column">
                                <h5 class="mb-1"> Customer: {{ $appointment->customer->account_firstname.' '.$appointment->customer->account_lastname  }}</h5>
                                <p class="mb-1 sm-text">Phone: {{ $appointment->customer->account_mobile }}</p>
                            </div>
                            <div class="right d-flex">
                                <div class="fa fa-comment-o"></div>
                                <div class="fa fa-phone"></div>
                            </div>
                        </div>
                                      
                        @if($appointment->appointment_status == 2)
                        <div class="row d-flex justify-content-between mx-2 px-3">
                            <button class="btn btn-white">Reschedule</button>
                            <a href="{{route('appointment-approve',$appointment->id)}}" class="btn btn-purple">Approve</a>
                        </div>
                        @else
                        <div class="row d-flex justify-content-between mx-2 px-3">
                        <p>Waiting visitation</p>
                        </div>
                        @endif
                    </div>
                </div>
            @endforeach
        </div>    
    
        
    
    
</x-Layout>