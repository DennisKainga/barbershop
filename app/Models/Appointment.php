<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;
    protected $fillable = [

        "appointment_customer_id",
        "appointment_barber_id",
        "appointment_date",
        "appointment_status"
    ];


    public function barber(){
        return $this->belongsTo(Account::class,"appointment_barber_id","id");
    }

    public function customer(){
        return $this->belongsTo(Account::class,"appointment_customer_id","id");
    }

    public function appointment_service(){
        return $this->hasMany(AppointmentService::class,"appointment_service_appointment_id");
    }


}
