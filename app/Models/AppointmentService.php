<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppointmentService extends Model
{
    use HasFactory;

    protected $fillable = [
        
        "appointment_service_appointment_id",
        "appointment_service_service_id"
    ];


    public function service(){
        return $this->belongsTo(Service::class,"id");
    }

    public function appointment(){
        return $this->belongsTo(Appointment::class,"id");
    }
}
