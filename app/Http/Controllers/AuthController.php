<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller{

    public function index(){
        return view("auth.index");
    }


    public function authenticate(Request $request){
        
        $credentials = $request->only('login_email', 'password');
 
        if(auth()->attempt($credentials)){
            
            $user = Auth::user();

            if ($user->login_rank == 0 && Auth::guard('admin')->attempt($credentials)) {
                // Authentication passed for admin...
                return redirect()->route('dashboard');

            } else if ($user->login_rank == 1 && Auth::guard('barber')->attempt($credentials)) {
                // Authentication passed for barber...
                return redirect()->route('dashboard');

            } else if($user->login_rank == 2 && Auth::guard('customer')->attempt($credentials)){
                // Authentication passed for customer...
                return redirect()->route('dashboard');
            }

        else {
            return redirect()->back()->withErrors(['error' => 'Invalid login credentials.']);
        }
        
    }else{
        // User not found in the database...
        return redirect()->back()->withErrors(['error' => 'Invalid login credentials.']);
    }

  
       
    }

   

    public function dashboard(){
        // $services = ;
        return view("dashboard.index",[

            "services"=>Service::all()
        ]);
    }

    public function destroy(Request $request){

   
        Auth::logoutCurrentDevice();
        
    

        $request->session()->invalidate();

        $request->session()->regenerateToken();

    

        return redirect()->route('show-login');
}
    //
}
