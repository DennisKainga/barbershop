<x-Layout>
    <div class="site-section bg-light">
        <div class="container">
          <div class="row justify-content-center  mb-1">
            <div class="col-md-7 text-center">
              <h3 class="page-heading text-center">{{ request()->segment(2) }}</h3>
              <p class="mb-0 lead">Manage {{ request()->segment(2) }}</p>
             
            </div>
          </div>
          <div class="row">
            <!-- Button trigger modal -->
          <button type="button" class="btn btn-info text-light mb-3 mx-auto" data-toggle="modal" data-target="#new-user">
            New {{ request()->segment(2) }}
          </button>
            <div class="col-12">
              <div class="d-flex mx-4">
                @foreach ($users as $user )
                <div class="item-1 mx-3" style="width:350px;">
                  {{-- <img src="{{asset('images/blank-user.png')}}" alt="Image" class="mx-auto" style="height:150px;object-fit:;"> --}}
                  <div class="card-header bg-dark text-light text-center" style="height:80px;">
                  <h1 class="text-light">Barberz</h1>
                  </div>
                  <div class="item-1-contents">
                    <h3>{{ $user->account_firstname .' '.$user->account_lastname }}</h3>
                    <ul>
                      <li class="d-flex"><span>E-mail</span> <span class="price ml-auto">{{ $user->user->login_email }}</span></li>
                      <li class="d-flex"><span>Phone</span> <span class="price ml-auto">{{ $user->account_mobile }}</span></li>
                      <li class="d-flex"><span>Residence</span> <span class="price ml-auto">{{ $user->account_location }}</span></li>
                    </ul>
                  <hr>
                    <div class="d-flex  mt-2 mb-3">
                      
                   
                     
                      <button type="button" class="btn btn-info text-light mx-auto" data-toggle="modal" data-target="#{{'update-user'.$user->id}}">
                        Edit
                      </button>
                      <form action="" method="post" class="d-inline">
                        <button type="button" class="btn btn-danger text-light mx-auto" data-toggle="modal" data-target="#new-user">
                          Delete
                        </button>
                      </form>
                    
                    </div>
                  </div>
                </div>  


                <div class="modal fade" id="{{'update-user'.$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add new User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <form method="POST" action="{{route('store-user')}}" >
                          @csrf
                          <input type="hidden" name="cat" value="{{ request()->segment(2) }}">
                
                          <div class="row mb-2">
                            <div class="col">
                              <label for="">First Name</label>
                              <input type="text" value="{{ $user->account_firstname }}" name="fname" class="form-control" >
                            </div>
                            <div class="col">
                              <label for="">Last Name</label>
                              <input type="text" value="{{ $user->account_lastname }}" name="lname" class="form-control">
                            </div>
                          </div>
                
                          <div class="row mb-2">
                            <div class="col">
                              <label for="">Phone</label>
                              <input type="text" value="{{ $user->account_mobile }}" name="phone" class="form-control">
                            </div>
                            <div class="col">
                              <label for="">Residence</label>
                              <input type="text" value="{{ $user->account_location }}" name="residence" class="form-control">
                            </div>
                          </div>
                
                          <div class="row mb-2">
                            <div class="col">
                              <label for="">Gender</label>
                              <input type="text" value="{{ $user->account_gender }}" name="gender" class="form-control">
                            </div>
                            <div class="col">
                              <label for="">Date of Birth</label>
                              <input type="date"  value="{{ $user->account_dob }}" name="dob" class="form-control">
                            </div>
                          </div>
                          <div class="row">
                
                            <div class="col">
                              <label for="">E-Mail</label>
                              <input type="text" value="{{ $user->user->login_email }}" name="email" class="form-control">
                            </div>
                          </div>
                          <div class="modal-footer ">
                            {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                            <button type="submit" class="btn btn-success text-light mx-auto">Update</button>
                          </div>
                        </form>
                      </div>
                    
                    </div>
                  </div>
                </div>
                @endforeach     
                     
              </div>
            </div>
          </div>
          <p class="text-center mt-3">
            {{ $users->links() }}
           </p>    
        </div>
    </div>




    <!-- Modal -->
<div class="modal fade" id="new-user" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add new User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{route('store-user')}}" >
          @csrf
          <input type="hidden" name="cat" value="{{ request()->segment(2) }}">

          <div class="row mb-2">
            <div class="col">
              <input type="text" name="fname" class="form-control" placeholder="First name">
            </div>
            <div class="col">
              <input type="text" name="lname" class="form-control" placeholder="Last name">
            </div>
          </div>

          <div class="row mb-2">
            <div class="col">
              <input type="text" name="phone" class="form-control" placeholder="Phone Number">
            </div>
            <div class="col">
              <input type="text" name="residence" class="form-control" placeholder="Residence">
            </div>
          </div>

          <div class="row mb-2">
            <div class="col">
              <input type="text" name="gender" class="form-control" placeholder="Gender">
            </div>
            <div class="col">
              <input type="date" name="dob" class="form-control" placeholder="Date Of Birth">
            </div>
          </div>
          <div class="row">

            <div class="col">
              <input type="text" name="email" class="form-control" placeholder="E-mail">
            </div>
          </div>
        
        
          <div class="modal-footer ">
            {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
            <button type="submit" class="btn btn-success text-light mx-auto">Submit</button>
          </div>
        </form>
      </div>
    
    </div>
  </div>
</div>
</x-Layout>