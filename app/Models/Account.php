<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;
    protected $fillable = [
        "account_firstname",
        "account_lastname",
        "account_mobile",
        "account_gender",
        "account_dob",
        "account_location",
        "account_login_id"
    ];

    public function user(){

        return $this->hasOne(User::class,"id","account_login_id");
    }

    public function service(){
        
        return $this->hasOne(Service::class, "service_account_id");
    }

    public  function appointment_barber(){
        return $this->hasMany(Appointment::class,"appointment_barber_id");
    }

    public  function appointment_customer(){
        return $this->hasMany(Appointment::class,"appointment_customer_id");
    }
}
