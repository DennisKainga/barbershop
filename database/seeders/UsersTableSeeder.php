<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
            User::firstOrCreate(
                    ['login_email' => 'redemptus@gmail.com'],
                    [
                        'password' =>  Hash::make('123'),
                        'login_rank'=> 0
                    ]
            );
        }
}
