<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Service;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($rank){
        if($rank == "customers"){
            // dd("hit");
            $rank = 2;

            $users = Account::whereHas('user', function($query) {
                $query->where('login_rank', 2);
            })->paginate(3);

            // $users = Account::whereHas('user', function ($query) use ($rank) {
            //     $query->where('login_rank',  $rank);
            // })->paginate(3);
        }
        else if($rank == 'barbers'){
            $rank = 1;
            $users = Account::whereHas('user', function($query) {
                $query->where('login_rank', 1);
            })->paginate(3);
        }


       
        return view("users.index",[
            "users"=>$users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request){
        // dd($request->cat);

        $FormFields = $request->validate([
            "fname"=> ["required"],
            "lname"=>["required"],
            "phone"=>["required"],
            "residence"=>["required"],
            "gender"=>["required"],
            "dob"=>["required"],
            "email"=>["required"]
        ]);
        $rank = null;
       
        if($request->cat == "customers"){
            $rank = 2;
        }
        else if($request->cat == "barbers"){
            $rank=1;
        }
        else{
            abort(403);
        }

        

        $login = User::create([
            "login_email"=>$FormFields["email"],
            "password"=> Hash::make($FormFields["email"]),
            "login_rank"=>$rank
        ]);

        Account::create([
            "account_firstname"=>$FormFields["fname"],
            "account_lastname"=>$FormFields["lname"],
            "account_mobile"=>$FormFields["phone"],
            "account_gender"=>$FormFields["gender"],
            "account_dob"=>$FormFields["dob"],
            "account_location"=>$FormFields["residence"],
            "account_login_id"=>$login->id
        ]);

        return redirect()->back();
      
    }

    /**
     * Display the specified resource.
     */
    public function show(Account $account){        
        return view("services.index",[
            "user"=>$account,
            "services"=>Service::where("service_account_id",$account->id)->get() 
        ]);
    
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
