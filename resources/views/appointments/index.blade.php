<x-Layout>

<style>

    .card {
        margin: 60px auto;
        border: none !important;
        width: 450px;
    }

    @media screen and (max-width: 768px) {
        .card {
            width: 350px;
        }
    }

    .card-strip {
        border-bottom: 1px solid lightgray;
        padding-top: 20px;
        padding-bottom: 20px;
    }

    img {
        width: 60px;
        height: 60px;
        border-radius: 50%;
        cursor: pointer;
    }

    .sm-text {
        font-size: 15px;
    }

    .fa {
        font-size: 30px;
        padding: 10px;
    }

    .fa-comment-o {
        color: gray;
        cursor: pointer;
    }

    .fa-phone {
        color: #1E88E5;
        padding-left: 20px;
        cursor: pointer;
    }

    .time {
        border: 1px lightgray solid;
        border-radius: 5px;
        font-weight: bold;
        padding: 1px 8px;
        margin: 0px 6px;
    }

    .price {
        font-size: 20px;
    }

    .text-muted {
        color: #BDBDBD !important;
    }

    button:focus {
        -moz-box-shadow: none !important;
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
        outline-width: 0;
    }

    .btn {
        width: 48%;
        height: 55px;
        margin: 20px 0px;
        font-weight: bold;
        letter-spacing: 1px;
    }

    .btn-white {
        background-color: #fff;
        color: #000;
        border: 1px gray solid;
    }

    .btn-white:hover {
        background-color: lightgray;
        color: #000;
    } 

    .btn-purple {
        background-color: #5E35B1;
        color: #fff;
        border: 1px #5E35B1 solid;
    }

    .btn-purple:hover {
        background-color: #311B92;
        color: #fff;
    }
</style>


@auth('barber')
    <div class="container-fluid px-1 py-5">
        @foreach ($appointments as $appointment)
            <div class="row d-flex justify-content-center ">
                <div class="card shadow-lg p-3 mb-5 bg-white rounded">
                    <div class="row d-flex justify-content-between mx-2 px-3 card-strip">
                        <div class="left d-flex flex-column">
                            {{-- <h5 class="mb-1">10:00 - 11:00 AM</h5> --}}
                            <p class="mb-1 sm-text">Date: {{ $appointment->appointment_date }}</p>
                        </div>
                    </div>
                    <div class="row d-flex justify-content-between mx-2 px-3 card-strip">
                        <div class="left d-flex flex-column">
                            <h5 class="mb-1"> Customer: {{ $appointment->customer->account_firstname.' '.$appointment->customer->account_lastname  }}</h5>
                            <p class="mb-1 sm-text">Phone: {{ $appointment->customer->account_mobile }}</p>
                        </div>
                        <div class="right d-flex">
                            <div class="fa fa-comment-o"></div>
                            <div class="fa fa-phone"></div>
                        </div>
                    </div>
                                  
                    @if($appointment->appointment_status == 1)
                    <div class="row d-flex justify-content-between mx-2 px-3">
                        <button class="btn btn-white">Reschedule</button>
                        <a href="{{route('appointment-approve',$appointment->id)}}" class="btn btn-purple">Approve</a>
                    </div>
                    @else
                    <div class="row d-flex justify-content-between mx-2 px-3">
                    <p>Waiting visitation</p>
                    </div>
                    @endif
                </div>
            </div>
        @endforeach
    </div>

@endauth
<!-- </x-Layout> -->


@auth('customer')
    <div class="container-fluid px-1 py-5">
        @if($appointment==null )
      
        <div class="mx-auto d-flex justidy-content-center">
            <button class="btn btn-info mx-auto w-25" data-toggle="modal" data-target="#new-app" >Create Appointment</button>
        </div>
        @endif
        @if($appointment!=null )
        @if($appointment->appointment_status == 1)
        <div class="mx-auto d-flex justidy-content-center">
            <button class="btn btn-info mx-auto w-25" data-toggle="modal" data-target="#app-service" >Choose Service</button>
        </div>
        @endif
        @endif
        @unless($appointment == null)
        <div class="row d-flex justify-content-center">
            <div class="card shadow-lg p-3 mb-5 bg-white rounded">
                <div class="row d-flex justify-content-between mx-2 px-3 card-strip">
                    <div class="left d-flex flex-column">
                        <h5 class="mb-1">Barber: {{ $appointment->barber->first()->account_firstname }}</h5>
                        {{-- <p class="text-muted mb-1 sm-text">FIRST VISIT</p> --}}
                    </div>
                    <div class="right d-flex">
                        <div class="fa fa-comment-o"></div>
                        <div class="fa fa-phone"></div>
                    </div>
                </div>
          
              @foreach ($selected_services as $selected_service)
                  
              <div class="row justify-content-between mx-2 px-3 card-strip">
                  <div class="left d-flex">
                      <h5 class="mb-1">{{ $selected_service->service_name }}</h5>
                      {{-- <span class="time">1 hr</span> --}}
                    </div>
                    <div class="right d-flex">
                        <p class="mb-0 price"><strong class="text">ksh: {{  $selected_service->service_price }}</strong></p>
                    </div>
                </div>
                @endforeach
               
                @if($appointment->appointment_status == 1)
                <div class="row d-flex justify-content-between mx-2 px-3">
                    <button class="btn btn-danger">Delete</button>
                    <a href="{{route('appointment-finalize',$appointment->id)}}"  class="btn btn-purple">Finalize</a>
                </div>
                @elseif($appointment->appointment_status == 2)
                <div class="row d-flex justify-content-between mx-2 px-3">
                   <p>Please wait as we work on your appointment</p>
                </div>
                @elseif($appointment->appointment_status == 3)
                <div class="row d-flex justify-content-between mx-2 px-3">
                    <p>Your appointment has been approved</p>
                 </div>

                @endif
            </div>
        </div>
        @endunless
    </div>


    <div class="modal fade" id="new-app" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Create an appointment</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form method="POST" action="{{route('appointment-store')}}" >
                @csrf      
                <div class="row mb-2">
                  <div class="col">
                    <input type="date" name="appointment_date" class="form-control" placeholder="Choose Date">
                  </div>
                 
                </div>

                <div class="row mb-2">
                    <div class="col">
                        <select name="barber" class="form-control form-select form-select-lg mb-3" aria-label="Default select example">
                            <option selected disabled>Select Barber</option>
                            @foreach ($barbers as $barber )
                            <option value="{{ $barber->id }}">{{ $barber->account_firstname." ".$barber->account_lastname }}</option>
                            @endforeach
                        </select>
                    </div>
                  
                  </div>
                          
                <div class="modal-footer ">
                  {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                  <button type="submit" class="btn btn-success text-light mx-auto">Submit</button>
                </div>
              </form>
            </div>
          
          </div>
        </div>
    </div>
    
    @unless($appointment == null)
    <div class="modal fade" id="app-service" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Choose service</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form method="POST" action="{{route('appointment-service-store')}}" >
                @csrf      
              <input type="hidden" name="appointment" value="{{ $appointment->id }}">
                <div class="row mb-2">
                    <div class="col">
                        <select name="service" class="form-control form-select form-select-lg mb-3" aria-label="Default select example">
                            <option selected disabled>Select service</option>
                            @foreach ($services as $service )
                            <option value="{{ $service->id }}">{{ $service->service_name." ksh: ".$service->service_price }}</option>
                            @endforeach
                        </select>
                    </div>
                  
                  </div>
                          
                <div class="modal-footer ">
                  {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                  <button type="submit" class="btn btn-success text-light mx-auto">Add</button>
                </div>
              </form>
            </div>
          
          </div>
        </div>
    </div>
    @endif
    @endunless
</x-Layout>
