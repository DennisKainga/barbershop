<x-Layout>
  <div class="site-section">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
           
              {{-- <h3 class="scissors text-center page-heading">{{ $user->account_firstname." ".$user->account_lastname }}</h3> --}}
              <button type="button" class="btn btn-info text-light mb-3 mx-auto" data-toggle="modal" data-target="#new-service">
                New service
              </button>
         
          </div>
      </div>
       
      <div class="row hair-style">

        @foreach($services as $service)
          <div class="col-lg-4 col-md-4 col-sm-6 col-12">
            <a href="#!" class="place">
              <img src={{$service->service_image ? asset('storage/'.$service->service_image) : asset("images/no-image.png")}} alt="Image placeholder">
                <h2>{{ $service->service_name }}</h2>
                <span>ksh {{ $service->service_price }}</span><br>
                {{-- <span>Barber: {{$service->account->account_firstname .' '.$service->account->account_lastname}}</span> --}}
            </a>
          
          </div>
          @endforeach
          </div>
      </div>
  </div>
 
  <div class="modal fade" id="new-service" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add new service</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form method="POST" action="{{route('store-service')}}" enctype="multipart/form-data" >
                @csrf      
               
                <div class="row mb-2">
                  <div class="col">
                    <input type="text" name="service_name" class="form-control" placeholder="Title">
                  </div>
                  <div class="col">
                    <input type="file" name="service_image" class="form-control" placeholder="image">
                  </div>
                </div>
                <div class="row mb-2">
                 
                  <div class="col">
                    <input type="text" name="service_price" class="form-control" placeholder="Enter price">
                  </div>
                </div>
                <div class="modal-footer ">
                  {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
                  <button type="submit" class="btn btn-success text-light mx-auto">Submit</button>
                </div>
            </form>
          </div>
        
        </div>
      </div>
    </div>

  </x-Layout>