<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\AppointmentServiceController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ServiceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get("/",function(){
//     return view("dashboard.index");
// });
Route::controller(AccountController::class)->group(function () {
    Route::prefix("users")->group(function(){
        Route::get("/{rank}",'index')->name('users-index');
        Route::post("store","store")->name('store-user');
    });
});


Route::prefix("services")->group(function(){

    Route::get("/", [ServiceController::class,'index'])->name("service-index");

    Route::get("service-barber/{account?}", [ServiceController::class, 'index'])->name("service-barber");

    Route::post("service-store", [ServiceController::class, 'store'])->name('store-service');

});

Route::prefix('appointments')->group(function(){
    
    Route::get("/{barber?}",[AppointmentController::class, 'index'])->name("appointment-index");

    Route::post("appointment-store",[AppointmentController::class, 'store'])->name('appointment-store');
    // finalize
    Route::get("appointment-finalize/{appointment}",[AppointmentController::class, 'finalize'])->name('appointment-finalize');

    Route::get("appointment-approve/{appointment}",[AppointmentController::class, 'approve'])->name("appointment-approve");

});

Route::prefix('appointment-service')->group(function(){

    Route::post("appointment-service-store", [AppointmentServiceController::class, "store"])->name("appointment-service-store");

});

Route::get("/", [ AuthController::class, "index"])->name("show-login");

Route::post("login", [AuthController::class, 'authenticate'])->name("authenticate");

Route::get("home", [AuthController::class, 'dashboard'])->name('dashboard');

Route::post('logout', [AuthController::class, 'destroy'])->name('logout')->middleware('auth');
