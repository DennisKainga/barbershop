@php
if(Auth::guard('admin')->check()){
  $routes = [
  [
    "route_to"=>"users",
    "route_name"=>"Customers",
    "params"=>"customers"
  ],

  [
    "route_to"=>"users",
    "route_name"=>"Barbers",
    "params"=>"barbers",
  ],

  [
  "route_to"=>"services",
  "route_name"=>"services",
  "params"=>null
  ]
];
}else if(Auth::guard('customer')->check()){

  $routes = [
  [
    "route_to"=>"appointments",
    "route_name"=>"appointments",
    "params"=>null
  ],
  [
  "route_to"=>"services",
  "route_name"=>"services",
  "params"=>null
  ]
];

}else if(Auth::guard('barber')->check()){

$routes = [
[
  "route_to"=>"appointments",
  "route_name"=>"appointments",
  "params"=>"barber"
],
[
"route_to"=>"services",
"route_name"=>"services",
"params"=>null
]
];

}


@endphp
<x-BaseLayout>
    <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
    </div>
      <header class="site-navbar site-navbar-target" role="banner">
        <div class="container">
          <div class="row align-items-center position-relative">
            <div class="col-3 ">
              <div class="site-logo">
                <a href="/">Barberz</a>
              </div>
            </div>
            <div class="col-9  text-right">
              <span class="d-inline-block d-lg-none"><a href="#" class="text-white site-menu-toggle js-menu-toggle py-5 text-white"><span class="icon-menu h3 text-white"></span></a></span>
              <nav class="site-navigation text-right ml-auto d-none d-lg-block" role="navigation">
                <ul class="site-menu main-menu js-clone-nav ml-auto ">
                  <li><a href="{{route('dashboard')}}" class="nav-link" >Home</a></li>
                  @foreach ($routes as $route )
                  <li><a href="{{"/".$route['route_to'].'/'.$route['params'] }}" class="nav-link">{{ $route["route_name"] }}</a></li>
                  @endforeach
                  <li>

                    <form action="{{route('logout')}}" method="post">
                      @csrf
                      <button type="submit" class="btn text-light" >Logout</button>    
                    </form>

                  </li>
                </ul>
              </nav>
            </div>            
          </div>
        </div>
      </header>
        <div class="ftco-blocks-cover-1">
            <div class="site-section-cover overlay" data-stellar-background-ratio="0.5" style="background-image: url('{{asset('images/hero_1.jpg')}}')">
            <div class="container">
                <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-7">
                    <h1 class="mb-3">More Than Just A Haircut</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta veritatis in tenetur doloremque, maiores doloribus officia iste. Dolores.</p>
                    <p><a href="#" class="btn btn-primary">Learn More</a></p>
                </div>
                </div>
            </div>
            </div>
        </div>
   {{ $slot }}
</x-BaseLayout>