<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($account=null){
        $user = null;

        if($account !=null){
            $user = Account::find($account);
            $services = Service::where('service_account_id',$account)->get();
        }
        else if($account == null){
            
            $services = Service::all();
        }

        return view("services.index",[
            "services"=>$services,
            "user"=>$user
        ]);
       
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request){

        $FormFields = $request->validate([

            "service_name"=>["required"],
            "service_price"=>["required"],   
        ]);

       

        // dd($request->hasFile("service_image"));
        if($request->hasFile("service_image")){

            $FormFields['service_image'] = $request->file('service_image')->store('service', 'public');
        }

        Service::create($FormFields);

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
